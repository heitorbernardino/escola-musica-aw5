package br.com.escolamusica.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.escolamusica.dao.CursoDAO;
import br.com.escolamusica.modelo.Curso;
import br.com.escolamusica.modelo.TipoCurso;

@ManagedBean
@SessionScoped
public class CursoController {
	
	private Curso curso = new Curso();
	private List<Curso> listaCursos = CursoDAO.listar();
	private Curso cursoExclusao;

	public String salvarCurso(){
		System.out.println("Salvando curso");
		CursoDAO.salvar(curso);
		listaCursos = CursoDAO.listar();
		curso = new Curso();
		return "ListaCursos?faces-redirect=true";
	}
	
	public String editar(Curso curso){
		this.curso = curso;
		return "CadastroCurso?faces-redirect=true";
	}
	
	public void prepararExclusao(Curso curso){
		cursoExclusao = curso;
	}
	
	public void excluir(){
		CursoDAO.excluir(cursoExclusao);
		listaCursos = CursoDAO.listar();
	}
	
	public String cancelar(){
		this.curso = new Curso();
		return "ListaCursos?faces-redirect=true"; 
	}
	
	public TipoCurso[] getTiposCurso(){
		return TipoCurso.values();
	}
	
	

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Curso> getListaCursos() {
		return listaCursos;
	}

	public void setListaCursos(List<Curso> listaCursos) {
		this.listaCursos = listaCursos;
	}

	
}
