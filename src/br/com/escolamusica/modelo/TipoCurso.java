package br.com.escolamusica.modelo;

public enum TipoCurso {

	CORDAS("Cordas"), MADEIRAS("Madeiras"), 
	METAIS("Metais"), PERCUSSAO("Percussão");
	
	private String label;
	
	TipoCurso(String label){
		this.label = label;
	}

	public String getLabel() {
		return label;
	};
	
	
}
